const express = require("express");
const router = express.Router();

const { db } = require("../util/admin");

router.post("/", function (req, res, next) {
  try {
    const username = req.body.email;
    exists(username, res).then((exist) => {
      if (exist === false) {
        let listLearnLangRef = addLanguagesRef(req.body.learnedLanguages);
        let pNativeLanguage = req.body.nativeLanguage;
        const nativeLangRef = db.collection("languages").doc(pNativeLanguage);
        nativeLangRef.get().then(() => {
          let documentToAdd = {
            birthDate: new Date(req.body.birthDate),
            email: req.body.email,
            lastname: req.body.lastname,
            learnedLanguages: listLearnLangRef,
            name: req.body.name,
            nativeLanguage: nativeLangRef,
            residenceCountry: req.body.residenceCountry,
            usedDuolingo: req.body.usedDuo,
          };
          db.collection("users")
            .doc(username)
            .set(documentToAdd)
            .then(
              res.status(200).json({
                message: "The user " + username + " was successfully signed in",
              })
            );
        });
      } else {
        res.status(400).json({ message: "The user already exists." });
      }
    });
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error ocurred when accessing the DB." });
  }
});

router.get("/:username", function (req, res, next) {
  let username = req.params.username;
  try {
    const docRef = db.collection("users").doc(username);
    docRef.get().then((doc) => {
      if (!doc.exists) {
        return res
          .status(400)
          .json({ message: "The user " + username + "doesn't exist." });
      } else {
        let data = doc.data();
        let seconds = data.birthDate._seconds;
        let nativeLanguage = data.nativeLanguage._path.segments[1];

        let learnedLanguages = data.learnedLanguages;
        let listLearnedLanguages = [];
        for (let i = 0; i < learnedLanguages.length; i++) {
          listLearnedLanguages.push(learnedLanguages[i]._path.segments[1]);
        }

        return res.status(200).json({
          birthDate: new Date(seconds * 1000).toLocaleDateString("en-US"),
          email: data.email,
          lastname: data.lastname,
          learnedLanguages: listLearnedLanguages,
          name: data.name,
          nativeLanguage: nativeLanguage,
          residenceCountry: data.residenceCountry,
        });
      }
    });
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error ocurred when accessing the DB." });
  }
});

function addLanguagesRef(learnedLanguages) {
  let listLearnLangRef = [];
  for (let i = 0; i < learnedLanguages.length; i++) {
    const langRef = db.collection("languages").doc(learnedLanguages[i]);
    listLearnLangRef.push(langRef);
  }
  return listLearnLangRef;
}

async function exists(username, res) {
  try {
    const usersRef = db.collection("users");
    let snapshot = await usersRef.get();
    const allUsers = snapshot.docs.map((doc) => doc.id);
    return new Promise((resolve, reject) => {
      resolve(allUsers.includes(username));
    });
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error ocurred when accessing the DB." });
  }
}

module.exports = router;
