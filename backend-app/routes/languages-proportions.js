const { db } = require("../util/admin");

let express = require("express");
let router = express.Router();



router.post("/", async function (req, res, next) {
  try {
    const dictRef = db
      .collection("users")
      .doc(req.body.user)
      .collection("dictionary");

    const refUser = db.collection("users").doc(req.body.user);

    let natLang = refUser.get().then((doc) => {
      if (!doc.exists) {
        return res
          .status(400)
          .json({ message: "The user " + username + "doesn't exist." });
      } else {
        let data = doc.data();
        let nativeLanguage = data.nativeLanguage._path.segments[1];
        return nativeLanguage;
      }
    });

    natLang = await natLang;
    natLang = String(natLang);

    dictRef.get().then((snapshot) => {
        var dictLang = {};
        let userLanguages = snapshot.docs.map((doc) => {
        let data = doc.data();
        let segments = data.originalLanguage._path.segments;
        let lang = segments[segments.length - 1];
        lang = String(JSON.stringify(lang)).replaceAll('"','');
        if (lang!= natLang){
          return lang;} 
      });

      //cuenta la cantidad de "idiomas" que no son undefined
      //los undefined eran el idioma nativo
      let numWords = 0;
      userLanguages.forEach(x => {
        if(typeof x !== "undefined"){
          numWords++;
        }
      });

      // iteracion para cuenta de repeticiones de lenguajes registro en diccionario
      userLanguages.forEach((l) => {
        if(typeof l !== "undefined"){
          l = String(JSON.stringify(l)).replaceAll('"','');
          if(!dictLang.hasOwnProperty(l)){
            dictLang[l] = 1;
          }
          else {
            dictLang[l] = dictLang[l]+1;
          }
        }
      });
    
      return res.status(200).json(dictLang);
    });
  } catch (error) {
    return res
      .status(500)
      .json({ general: "Something went wrong, please try again" });
  }
});

module.exports = router;