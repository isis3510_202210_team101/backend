let express = require("express");
const config = require("../config.js");
const { Translate } = require("@google-cloud/translate").v2;
let router = express.Router();

const vision = require("@google-cloud/vision");
const client_vision = new vision.ImageAnnotatorClient();

const translate = new Translate();

// Object Detection to get the labels in the image
router.post("/", async function detectlabels(req, res, next) {
  const rta = [];
  try {
    const request_img = { image: req.body.image };
    const vision = require("@google-cloud/vision");
    const client_vision = new vision.ImageAnnotatorClient();
    const [result] = await client_vision.labelDetection(request_img);
    const labels = result.labelAnnotations;
    if (labels.length > 0) {
      const predictions_strings = [];
      for (let i = 0; i < labels.length; i++) {
        predictions_strings[i] = labels[i].description;
      }
      let [translations_objective] = await translate.translate(
        predictions_strings,
        req.body.objective
      );
      let [translate_native] = await translate.translate(
        predictions_strings,
        req.body.native
      );
      for (let i = 0; i < translate_native.length; i++) {
        let lowerTransNative = translate_native[i].toLowerCase();
        let capTransNative =
          lowerTransNative.charAt(0).toUpperCase() + lowerTransNative.slice(1);
        let lowerTransObjective = translations_objective[i].toLowerCase();
        let capTransObjective =
          lowerTransObjective.charAt(0).toUpperCase() +
          lowerTransObjective.slice(1);
        rta[i] = {
          native: capTransNative,
          objective: capTransObjective,
        };
      }
    }
    res.send(JSON.stringify(rta));
  } catch (error) {
    res.send(JSON.stringify(rta));
  }
});

module.exports = router;
