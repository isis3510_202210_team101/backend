let express = require("express");
const fetch = require("cross-fetch");
const { db } = require("../util/admin");
const config = require("../config.js");
const { Translate } = require("@google-cloud/translate").v2;
const { Firestore } = require("@google-cloud/firestore");
let router = express.Router();

const vision = require("@google-cloud/vision");
const client_vision = new vision.ImageAnnotatorClient();
const translate = new Translate();

// Landmark Detection from image
router.post("/", async function detectlandmark(req, res, next) {
  try {
    const image_uri = req.body.image.source.imageUri;
    const vision = require("@google-cloud/vision");
    const client_vision = new vision.ImageAnnotatorClient();
    const [result] = await client_vision.landmarkDetection(image_uri);
    const landmarks = result.landmarkAnnotations;
    if (landmarks.length > 0) {
      // Landmark detection
      const bestLandmark = landmarks[0];
      let rta = {
        englishName: bestLandmark.description,
        score: bestLandmark.score,
        location_lat: bestLandmark.locations[0]["latLng"]["latitude"],
        location_long: bestLandmark.locations[0]["latLng"]["longitude"],
        visits: 1,
      };
      // Landmark asociation to the user
      db.collection("users")
        .doc(req.body.user)
        .collection("visitedLandmarks")
        .doc(req.body.user + "-" + rta["englishName"])
        .set({
          date: new Date(),
          photo: req.body.image.source.imageUri,
        });
      // Validation if the landmark exists
      const landmarkRef = db.collection("landmarks").doc(rta["englishName"]);
      landmarkRef.get().then((snapshot) => {
        let documentToAdd = {
          location: new Firestore.GeoPoint(
            rta["location_lat"],
            rta["location_long"]
          ),
          visits: 1,
        };
        const landmarkInfo = snapshot.data();
        if (landmarkInfo) {
          // Setting information for the service answer
          rta["description"] = landmarkInfo["description"];
          rta["originalLanguage"] = landmarkInfo["originalLanguage"];
          rta["originalName"] = landmarkInfo["originalName"];
          rta["visits"] = landmarkInfo["visits"] + 1;
          res.send(JSON.stringify([rta]));
          // Document update of visits
          documentToAdd["description"] = landmarkInfo["description"];
          documentToAdd["originalLanguage"] = landmarkInfo["originalLanguage"];
          documentToAdd["originalName"] = landmarkInfo["originalName"];
          documentToAdd["visits"] = landmarkInfo["visits"] + 1;
          db.collection("landmarks").doc(rta["englishName"]).set(documentToAdd);
          landmarksStatistics(documentToAdd, rta["englishName"]);
        } else {
          // Wikipedia search
          let englishName = bestLandmark.description.split(" ").join("%20");
          let url =
            "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&exsentences=2&redirects=1&titles=" +
            englishName;
          fetch(url, {
            headers: { "Api-User-Agent": "LangApp/1.0" },
          })
            .then((res) => res.json())
            .then((obj) => {
              let pages = obj.query.pages;
              if (!("-1" in pages)) {
                // Description asociation
                let [value] = Object.values(pages);
                rta["description"] = value["extract"];
                let regExp = /\(([^)]+)\)/;
                let inside_parentesis = regExp.exec(rta["description"])[1];
                let parts = inside_parentesis.split(";");
                let desired_part = "";
                for (let i = 0; i < parts.length; i++) {
                  if (parts[i].includes(":")) {
                    desired_part = parts[i];
                    break;
                  }
                }
                parts = desired_part.split(":");
                // Setting information for the service answer
                rta["originalLanguage"] = parts[0].trim();
                rta["originalName"] = parts[1].trim();
                res.send(JSON.stringify([rta]));
                // Document creation
                documentToAdd["description"] = rta["description"];
                documentToAdd["originalLanguage"] = rta["originalLanguage"];
                documentToAdd["originalName"] = rta["originalName"];
                db.collection("landmarks")
                  .doc(rta["englishName"])
                  .set(documentToAdd);
                landmarksStatistics(documentToAdd, rta["englishName"]);
              } else {
                res.status(404).json({
                  message: "The detected landmark was not founded.",
                });
              }
            });
        }
      });
    } else {
      res.status(404).json({
        message: "There was not detected any landmark.",
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "An error ocurred when accessing the DB.",
    });
  }

}

);

function landmarksStatistics(newLandmark, englishName) {
  const dictRef = db
    .collection("statisticsLandmarks")
    .doc("popular").get().then((snapshot) => {
      let currentPopular = snapshot.data();
      if (newLandmark.visits > currentPopular.visits) {
        let documentToAdd = {};
        documentToAdd["first"] = db.collection("landmarks")
          .doc(englishName);
        documentToAdd["visits"] = newLandmark.visits;
        db.collection("statisticsLandmarks")
          .doc("popular")
          .set(documentToAdd);
      }
    });
}


module.exports = router;
