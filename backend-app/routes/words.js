const { db } = require("../util/admin");
const { Firestore } = require("@google-cloud/firestore");
const { Translate } = require("@google-cloud/translate").v2;
const fetch = require("cross-fetch");
const imageThumbnail = require("image-thumbnail");
const FormData = require("form-data");

const translate = new Translate({
  projectId: "centering-abode-345017",
  keyFilename:
    __dirname + "/translationAPI_key/centering-abode-345017-cfed0937f2e8.json",
});

let express = require("express");

let router = express.Router();

/**
 * Creates a word in the DB with its translations and associates it with the user's dictionary.
 */
router.post("/", function (req, res, next) {
  try {
    let lowerWord = req.body.word.toLowerCase();
    const capWord = lowerWord.charAt(0).toUpperCase() + lowerWord.slice(1);
    const languageRef = db
      .collection("languages")
      .doc(req.body.learningLanguage);
    languageRef
      .get()
      .then((snapshot) => {
        const languageInfo = snapshot.data();
        getEnglishWord(languageInfo, capWord)
          .then((englishWord) => {
            existsInDictionary(englishWord, req.body.user, res)
              .then((exist) => {
                if (exist === false) {
                  getValidTranslations(englishWord)
                    .then((translationsToInsert) => {
                      if (translationsToInsert.length === 0) {
                        res.sendStatus(404);
                      } else {
                        // Se crea la palabra
                        db.collection("words")
                          .doc(englishWord)
                          .set({ audio: "" })
                          .then(() => {
                            // Se añade la palabra en el diccionario
                            let wordReference = db
                              .collection("words")
                              .doc(englishWord);
                            let geopoint = new Firestore.GeoPoint(
                              req.body.latitude,
                              req.body.longitude
                            );
                            let documentToAdd = {
                              customTranslation: capWord,
                              location: geopoint,
                              originalLanguage: languageRef,
                              word: wordReference,
                            };
                            if (req.body.photo !== "") {
                              const form = new FormData();
                              const filename = Date.now().toString() + ".jpeg";
                              const baseFireStorage =
                                "https://firebasestorage.googleapis.com/v0/b/biz-lang.appspot.com/o";
                              const uri =
                                baseFireStorage +
                                "?uploadType=media&name=" +
                                filename;
                              imageThumbnail({
                                uri: req.body.photo,
                                responseType: "base64",
                              })
                                .then((buffer) => {
                                  form.append("file", buffer, {
                                    contentType: "image/jpeg",
                                    name: "file",
                                    filename: filename,
                                  });
                                  fetch(uri, {
                                    method: "POST",
                                    body: form,
                                  })
                                    .then((res) => res.json())
                                    .then((json) => {
                                      // Se crea la nueva palabra con thumbnail
                                      documentToAdd["thumbnail"] =
                                        baseFireStorage +
                                        "/" +
                                        filename +
                                        "?alt=media&token=" +
                                        json["downloadTokens"];
                                      db.collection("users")
                                        .doc(req.body.user)
                                        .collection("dictionary")
                                        .doc(req.body.user + "-" + englishWord)
                                        .set(documentToAdd);
                                    })
                                    .catch((e) =>
                                      res.sendStatus(500).json({
                                        message:
                                          "An error ocurred when uploading the thumbnail to FireStorage.",
                                      })
                                    );
                                })
                                .catch((e) =>
                                  res.sendStatus(500).json({
                                    message:
                                      "An error ocurred when creating the thumbnail.",
                                  })
                                );
                            } else {
                              // Se crea la nueva palabra sin thumbnail
                              documentToAdd["thumbnail"] = "";
                              db.collection("users")
                                .doc(req.body.user)
                                .collection("dictionary")
                                .doc(req.body.user + "-" + englishWord)
                                .set(documentToAdd);
                            }
                            // Se crea la referencia a la foto en tamaño original en FireBase
                            db.collection("users")
                              .doc(req.body.user)
                              .collection("dictionaryPhotos")
                              .doc(req.body.user + "-" + englishWord)
                              .set({ photo: req.body.photo });
                            // Se crean las traducciones
                            translationsToInsert.forEach((translation) => {
                              db.collection("translations")
                                .doc(translation.id)
                                .set(translation.translation);
                              db.collection("translationsDescription")
                                .doc(translation.id)
                                .set(translation.description);
                            });
                            res.status(200).json({
                              message:
                                "The word " +
                                englishWord +
                                " was added with its translations.",
                            });
                          })
                          .catch((error) =>
                            res.sendStatus(500).json({
                              message:
                                "An error ocurred when getting the translations of the word.",
                            })
                          );
                      }
                    })
                    .catch((error) => {
                      res.sendStatus(500).json({ message: error });
                    });
                } else {
                  res.status(400).json({
                    message: "Word already in your dictionary.",
                  });
                }
              })
              .catch((error) =>
                res.sendStatus(500).json({
                  message:
                    "An error ocurred when checking if the word already was in the user's dictionary.",
                })
              );
          })
          .catch((error) =>
            res.sendStatus(500).json({
              message:
                "An error ocurred when getting the English translation of the word.",
            })
          );
      })
      .catch((error) =>
        res.sendStatus(500).json({
          message: "An error ocurred when getting the selected language.",
        })
      );
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error ocurred when accessing the DB." });
  }
});

/**
 * Retorna el diccionario de palabras del usuario dado con las traducciones del idioma que está aprendiendo.
 */
router.post("/user", function (req, res, next) {
  try {
    const dictRef = db
      .collection("users")
      .doc(req.body.user)
      .collection("dictionary");
    dictRef.get().then((snapshot) => {
      const userWords = snapshot.docs.map((doc) => {
        let data = doc.data();
        let segments = data.word._path.segments;
        let word = segments[segments.length - 1];
        return { word: word, thumbnail: data.thumbnail };
      });
      const transRef = db.collection("translations");
      transRef.get().then((snapshot) => {
        const translations = snapshot.docs.map((doc) => ({
          id: doc.id,
          translation: doc.data().translation,
        }));
        let userDictList = userWords.map((word) => {
          for (let i = 0; i < translations.length; i++) {
            let partes = translations[i].id.split("-");
            if (
              partes[0] === req.body.learningLanguage &&
              partes[1] === word.word
            ) {
              return {
                word: word.word,
                thumbnail: word.thumbnail,
                translation: translations[i].translation,
              };
            }
          }
        });
        return res.status(200).json(userDictList);
      });
    });
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error ocurred when accessing the DB." });
  }
});

/**
 * Retorna la información detallada de una palabra particular: descripción y lenguaje original.
 */
router.post("/info", function (req, res, next) {
  try {
    const dictWordRef = db
      .collection("users")
      .doc(req.body.user)
      .collection("dictionary")
      .doc(req.body.user + "-" + req.body.word);
    dictWordRef.get().then((snapshot) => {
      const originalLanguage =
        snapshot.data().originalLanguage._path.segments[1];
      const languageRef = db.collection("languages").doc(originalLanguage);
      languageRef.get().then((snapshot) => {
        const languageInfo = snapshot.data();
        const transRef = db
          .collection("translations")
          .doc(req.body.learningLanguage + "-" + req.body.word);
        transRef.get().then((snapshot) => {
          let description = snapshot._fieldsProto.description;
          return res.status(200).json({
            description: description,
            originalLanguage: languageInfo,
          });
        });
      });
    });
  } catch (error) {
    return res
      .status(500)
      .json({ message: "An error ocurred when accessing the DB." });
  }
});

/**
 * Borra la palabra dada del diccionario del usuario
 */
router.delete("/user", function (req, res, next) {
  try {
    let lowerWord = req.body.englishWord.toLowerCase();
    const capWord = lowerWord.charAt(0).toUpperCase() + lowerWord.slice(1);
    db.collection("users")
      .doc(req.body.user)
      .collection("dictionary")
      .doc(req.body.user + "-" + capWord)
      .delete()
      .then(() =>
        res.status(204).json({ message: "Successful removal from dictionary." })
      );
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ message: "An error ocurred when accessing the DB." });
  }
});

/**
 * Valida si la palabra dada existe en el diccionario de palabras del usuario
 */
async function existsInDictionary(englishWord, user, res) {
  try {
    const dictWordRef = db
      .collection("users")
      .doc(user)
      .collection("dictionary");
    let snapshot = await dictWordRef.get();
    const userWords = snapshot.docs.map((doc) => doc.id.split("-")[1]);
    return new Promise((resolve, reject) => {
      resolve(userWords.includes(englishWord));
    });
  } catch (error) {
    res.status(500).json({
      message: "An error ocurred when accessing the DB.",
    });
  }
}

/**
 * Retorna la palabra traducida en inglés con base en la información del lenguaje en la que se encuentra
 */
async function getEnglishWord(languageInfo, capWord) {
  let englishWord = "";
  if (languageInfo.locale === "en") {
    englishWord = capWord;
  } else {
    let translations = await translate.translate(capWord, {
      from: languageInfo.locale,
      to: "en",
    });
    translations = Array.isArray(translations) ? translations : [translations];
    lowerWord = translations[0].toLowerCase();
    englishWord = lowerWord.charAt(0).toUpperCase() + lowerWord.slice(1);
  }
  return new Promise((resolve, reject) => {
    resolve(englishWord);
  });
}

/**
 * Crea las traducciones de la palabra en inglés en el resto de lenguajes junto con sus descripciones.
 * Si no tiene descripción en ninguno de los idiomas, no se crea.
 */
async function getValidTranslations(englishWord) {
  try {
    let translationsToInsert = [];
    let translationsWithDescription = 0;
    let englishWordReference = db.collection("words").doc(englishWord);
    let languagesRef = db.collection("languages");
    let snapshot = await languagesRef.get();
    let languages = snapshot.docs.map((doc) => ({
      language: doc.id,
      locale: doc.data().locale,
    }));
    for (let i = 0; i < languages.length; i++) {
      let localLangRef = db.collection("languages").doc(languages[i].language);
      let translation = englishWord;
      if (languages[i].locale !== "en") {
        let translations = await translate.translate(englishWord, {
          from: "en",
          to: languages[i].locale,
        });
        translations = Array.isArray(translations)
          ? translations
          : [translations];
        let lowerTranslation = translations[0].toLowerCase();
        translation =
          lowerTranslation.charAt(0).toUpperCase() + lowerTranslation.slice(1);
      }
      let url =
        "https://" +
        languages[i].locale +
        ".wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&exsentences=2&redirects=1&titles=" +
        translation;
      let pages = (
        await fetch(url, {
          headers: { "Api-User-Agent": "LangApp/1.0" },
        }).then((res) => res.json())
      ).query.pages;
      let description = "";
      if (!("-1" in pages)) {
        let [value] = Object.values(pages);
        description = value.extract;
        translationsWithDescription = translationsWithDescription + 1;
      }
      translationsToInsert.push({
        id: languages[i].language + "-" + englishWord,
        translation: {
          englishWord: englishWordReference,
          language: localLangRef,
          learningScore: 0,
          translation: translation,
        },
        description: { description: description },
      });
    }
    if (translationsWithDescription === 0) {
      translationsToInsert = [];
    }
    return new Promise((resolve, reject) => {
      resolve(translationsToInsert);
    });
  } catch (error) {
    return new Promise((resolve, reject) => {
      reject("An error ocurred when accessing the DB.");
    });
  }
}

module.exports = router;
