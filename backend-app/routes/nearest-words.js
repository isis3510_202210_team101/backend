const { db } = require("../util/admin");
let haversine = require("haversine-distance");

let express = require("express");
let router = express.Router();

router.post("/", function (req, res, next) {
  try {
    const dictRef = db
      .collection("users")
      .doc(req.body.user)
      .collection("dictionary");
    dictRef
      .get()
      .then((snapshot) => {
        let nearestWords = [];
        let userWords = snapshot.docs.map((doc) => {
          let data = doc.data();
          let segments = data.word._path.segments;
          let word = segments[segments.length - 1];
          return { word: word, location: data.location };
        });
        let userLocation = [req.body.latitude, req.body.longitude];
        userWords.forEach((word) => {
          if (
            haversine(
              [word.location._latitude, word.location._longitude],
              userLocation
            ) < 100
          ) {
            nearestWords.push(word);
          }
        });
        return res.status(200).json(nearestWords);
      })
      .catch((error) =>
        res.status(500).json({
          message: "An error occurred when accessing the user's dictionary.",
        })
      );
  } catch (error) {
    return res
      .status(500)
      .json({ general: "An error ocurred when accessing the DB." });
  }
});

module.exports = router;
