const { db } = require("../util/admin");

let express = require("express");
let router = express.Router();

//si en learnedLanguages hay lenguajes que no tienen palabras en dic, incluirlo en forgotten.
router.post("/", async function (req, res, next) {
  try {
    const dictRef = db
      .collection("users")
      .doc(req.body.user)
      .collection("dictionary");
    const refUser = db.collection("users").doc(req.body.user);

    let [learnedLanguages, natLang] = await refUser.get().then((doc) => {
      if (!doc.exists) {
        return res
          .status(400)
          .json({ message: "The user " + username + " doesn't exist." });
      } else {
        let data = doc.data();
        let nativeLang = data.nativeLanguage._path.segments[1];
        let learnedLang = data.learnedLanguages;
        learnedLang = learnedLang.map((l) => l._path.segments[1]);
        return [learnedLang, nativeLang];
      }
    });

    dictRef.get().then((snapshot) => {
      let languagesDict = snapshot.docs.map((doc) => {
        let data = doc.data();
        let segments = data.originalLanguage._path.segments;
        let lang = segments[segments.length - 1];
        //lang = String(JSON.stringify(lang)).replaceAll('"', "");
        if (lang != natLang) {
          return lang;
        }
      });

      //cuenta la cantidad de "idiomas" que no son undefined
      //los undefined eran el idioma nativo
      let dictLang = {};
      learnedLanguages.forEach((l) => {
        dictLang[l] = 0;
      });

      let numWords = 0;
      languagesDict.forEach((l) => {
        if (typeof l !== "undefined") {
          numWords++;
          dictLang[l] = dictLang[l] + 1;
        }
      });

      let forgottenLanguages = [];
      for (let i = 0; i < learnedLanguages.length; i++) {
        let localLang = learnedLanguages[i];
        if (dictLang[localLang] < numWords * 0.1) {
          forgottenLanguages.push(localLang);
        }
      }
      return res.status(200).json(forgottenLanguages);
    });
  } catch (error) {
    return res
      .status(500)
      .json({ general: "Something went wrong, please try again" });
  }
});

module.exports = router;
