require("dotenv").config({ path: "./.env" });
const config = require("./config.js");
let createError = require("http-errors");
let express = require("express");
let path = require("path");
let cookieParser = require("cookie-parser");
let logger = require("morgan");

let indexRouter = require("./routes/index");
let usersRouter = require("./routes/users");
let ObjectDetectionRouter = require("./routes/objectDetection");
let LandmarkDetectionRouter = require("./routes/landmarkDetection");
let wordsRouter = require("./routes/words");
let forgottenRouter = require("./routes/forgotten-languages");
let proportionsRouter = require("./routes/languages-proportions");

// Vision API
const vision = require("@google-cloud/vision");
const client = new vision.ImageAnnotatorClient({
  keyFilename: "./VisionAPI.json",
});

let nearestWordsRouter = require("./routes/nearest-words");

let app = express();
app.use(express.static(__dirname + "public"));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/objectDetection", ObjectDetectionRouter);
app.use("/landmarkDetection", LandmarkDetectionRouter)
app.use("/words", wordsRouter);
app.use("/nearest", nearestWordsRouter);
app.use("/forgot", forgottenRouter);
app.use("/proportions", proportionsRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
