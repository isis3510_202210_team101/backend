const admin = require("firebase-admin");

const serviceAccount = require("../firebase_key/biz-lang-firebase-adminsdk-f2pp2-4f59fc64d9.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();
module.exports = { admin, db };